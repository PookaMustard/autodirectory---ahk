﻿#NoEnv
SetWorkingDir %A_ScriptDir%

Loop, Files, *.*
{
	if (A_LoopFileName != A_ScriptName) ; If the current file isn't this script
	{
        StringSplit, Letters, A_LoopFileName
        Letter = %Letters1% ; Obtain first letter of file
        if InStr(ForbiddenLetters, Letter) ; If letter is part of forbidden letters, ignore
            continue
        
        if InStr("1234567890", Letter) ; Use a hashtag instead of numbers
            Letter = #
        else
            if !InStr("abcdefghijklmnopqrstuvwxyz", Letter) ; If it's not a number or a letter, use an underscore
                Letter = _
        
        If A_LoopFileName = %Letter% 
        {
            ForbiddenLetters .= Letter
            ErrorMsg = True
            continue
        } ; If a file name is only a single letter, add it to forbidden letters and skip all files that begin with that letter
        
        If !FileExist(%Letter%) ; Create folder for that letter if it doesn't exist
            FileCreateDir, %Letter%
            
        FileMove, %A_LoopFileName%, %Letter%, 0 ; Move files
    }
}

	If ErrorMsg
		MsgBox, Some files were not moved! Check your directory for any files with single-letter names and no extensions.
