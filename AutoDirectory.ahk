﻿#NoEnv
SetWorkingDir %A_ScriptDir%

Loop, Files, *.* ; Loop through files
{
	if (FileName != A_ScriptName) ; If the current file isn't this script
    {
        if A_LoopFileName contains _
        {
            StringSplit, File, A_LoopFileName, ["_","."]
            FileName = %File1%
        } ; Obtain the name of the file before the underscores.
        if !FileExist(FileName) ; If a folder doesn't exist with the obtained name, create it.
            FileCreateDir, %FileName% 
        FileMove, %A_LoopFileName%, %FileName%, 0 ; Move file to folder.
	}
}