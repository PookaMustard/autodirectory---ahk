AutoDirectory is an AutoHotKey script that moves files to named folders for convenience. There are two versions of the script.

## AutoDirectory

Moves similarly named files to a folder. For example, "screenshot_2019_28_01" and "screenshot_2018_24_05" would be moved to a folder called "screenshot"

## AutoDirectory Lettered

Moves files depending on their first letter. For example, "Android.png" and "Apple.jpg" would be moved to the folder "A"